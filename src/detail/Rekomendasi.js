import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'jquery';
import 'popper.js';
import './css/detail.css';
import produkRekomendasi from '../detail/img/detail-produk-rekomendasi.png';

function Rekomendasi(){
    return(
        
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                                <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o"  aria-hidden="true"></i>
                                <i class="fa fa-star-o"  aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                                <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o"   aria-hidden="true"></i>
                                <i class="fa fa-star-o"  aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                            <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i
                                    class="fa fa-star"
                                    
                                    aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o"  aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                                <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o"  aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                                <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-star"  aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o"  aria-hidden="true"></i>
                                <i class="fa fa-star-o"  aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 py-2 px-0">
                    <div class="card p-2 mx-1">
                        <a href="#">
                            <div class="recomendation">
                                <img src={produkRekomendasi} />
                            </div>
                        </a>
                        <div class="card-text">
                            Asus VivoBook Max M4 
                            <p class="cyan">Rp 6,000,000</p>
                            <div class="grey">
                                <i class="fa fa-fw fa-heart-o" class="fontSize7"></i>
                                <span class="rating">
                                    2770
                                </span>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-fw fa-star-half-o" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                <span class="rating">(2771)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Rekomendasi;